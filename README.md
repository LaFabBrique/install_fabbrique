## /!\ W.I.P... /!\

Script de post-installation des postes informatiques de **La Fab'Brique**, FabLab associatif de Salies-de-Béarn (64)

contact[@]lafabbrique[.]org - http://lafabbrique.org - git: https://framagit.org/LaFabBrique/install_fabbrique.git

_info: installation sur Debian 9/Ubuntu 16.04/Mint 8_

### Téléchargements/installations
  - **IDE Arduino** _(depuis le site. V1.6 pour compatibilité Arduino "clones chinois" - binaires dans '~/.local/bin/', lanceur dans '~/.local/applications')_
    - ArduBlock _(depuis le site)_
  - **FreeCad** _(depuis les dépots)_ + **Plugins** _(depuis un PPA)_:
    - BOLTS (bibliothèque produits techniques)
	- assembly2 (assemblages)
	- sheetmetal (travail des métaux en feuilles)
	- animation (animer des assemblages 3D)
  - **Scratch** _(depuis les dépots)_
  - **XOD** _(depuis le site, arch 64bits uniquement)_
  - **Fritzing** _(depuis les dépots)_
  - **Processing** _(depuis le site)_
  - ...

### Personnalisations
  - **Firefox**
    - Import du profil "lafabbrique" (en cours)
  - Fonds d'écran

## To-Do
  - Vérification de la présence des dossiers en cas de binaires depuis les sites
  - ~~Passer les URLs en variables (pour faciliter la maitenance)~~
  - "_Fins propres_" du script (en cas d'ER)
  - ~~Sélection auto de ''arch''~~
  - Arduino
    - ~~Ajout ArduBlock~~
    - Installer la dernière version aussi (pour les "vrais" cartes), et différencier les lanceurs
    - ...
  - Freecad
    - ~~Inclure les plugins intéressants~~
    - Proposer la compilation (pour avoir la dernière version) ?
    - ...
  - Scratch
    - ~~Scratch 2.0?~~ (nécessite Adobe Air! OK en ligne...)
    - [BYOB](http://byob.berkeley.edu/)/[Panther](http://pantherprogramming.weebly.com/index.html) (modifs Scratch avec des fonctions supplémentaires)? _(à tester)_
    - Support Arduino ([S4A](http://s4a.cat/))? _(utilité si ArduBlock installé?)_
  - XOD
    - Erreur de téléchargement du paquet depuis l'URL du site ?? (googleapis...)
    - ...
  - Fritzing
    - Proposer la compilation (pour avoir la dernière version) ?
    - ...
  - Intégration à faire:
    - Processing (en cours...)
    - Python IDE
    - ...

## Licence
[Licence Puplique Rien À Branler](LICENSE.md) ([WTFPL](http://www.wtfpl.net/))
