#!/bin/bash

## Srcipt d'installation des programmes basiques pour les postes de
## La Fab'Brique, FabLab associatif de Salies-de-Béarn (64)
##
## contact[@]lafabbrique[.]org - http://lafabbrique.org
## git: https://framagit.org/LaFabBrique/install_fabbrique.git
##
## 1) Vérifie les mises à jour, télécharge et installe quelques paquets "basiques"
## 2) Installe le profil "lafabbrique" pour Firefox
## 3) Télécharge dans '$HOME/Images' les fonds d'écran La Fab'Brique
## 4) Télécharge et installe les applications suivantes:
##		- IDE Arduino
##			- V1.6.13 pour la compatibilité avec les "clones asiatiques" d'Arduino
##			- binaire dans $HOME/.local/bin/ et des lanceurs dans $HOME/.local/applications)
##          - ArduBlock (interface graphique de programmation tel Scratch, dédiée à Arduino)
##		- FreeCad + Plugins
##			- BOLTS (bibliothèque technique)
##			- assembly2 (assemblages)
##			- sheetmetal (travail des métaux en feuilles)
##			- animation (animer des assemblages 3D)
##			- ...
##		- Scratch (interface graphique de programmation)
##		- Fritzing (croquis/schémas électroniques)
##		- XOD (interface graphique de programmation Arduino)
##		- Processing (croquis/schémas électroniques et programmation)

# Dossiers
##########
REPINSTALL=$HOME/.local/bin
REPTMP=`mktemp --directory`
REPFF=$HOME/.mozilla/firefox/
REPFFPERSO=$REPFF/lafabbrique
DIRARDUBLOCK=$HOME/Arduino/tools/ArduBlockTool/tool

# URLs
######
URLFF=	#Profil FireFox
URLWP=	#Fonds d'écrans
#arduino
URLARDUINO="https://downloads.arduino.cc"
ARDU64="$URLARDUINO/arduino-1.6.13-linux64.tar.xz"
ARDU32="$URLARDUINO/arduino-1.6.13-linux32.tar.xz"
URLARDUBLOCK="http://sourceforge.net/projects/ardublock/files/ardublock-all-20130712.jar"
#xod
URLXOD=""
#processing
URLPROCESSING="http://download.processing.org"
PROCESSING64="$URLPROCESSING/processing-3.3.6-linux64.tgz"
PROCESSING32="$URLPROCESSING/processing-3.3.6-linux32.tgz"

# Divers
########
ARCHITECTURE=$(arch)
BASICS="firefox firefox-locale-fr geany htop git-core unzip"
FREECADPLUGINS="freecad-extras-bolts freecad-extras-animation freecad-extras-sheetmetal freecad-extras-assembly2"

# Mises à jour
##############
echo "Mises à jour"
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install "$BASICS"
git config --global user.email "contact@lafabbrique.org" && git config --global user.name "lafabbrique"

echo "Architecture détectée: $ARCHITECTURE"
echo "Répertoire temporaire: $REPTMP"

# Vérification de $HOME/.local/bin
##############################
if [ -d "$REPINSTALL" ]; then
	echo "Le répertoire d'installation existe"
	
else
	echo "Création du répertoire d'installation"
	mkdir -p "$REPINSTALL"
fi

cd "$REPTMP"

# Import profil Firefox
#######################
if [ -d $REPFFPERSO ]; then
	echo "Le profil Firefox est à jour"
	
else
	echo "Import du profil Firefox"
	wget $URLFF
	unzip lafabbrique.zip
	mv lafabbrique $REPFF
	echo "Profil Firefox importé"
fi

# Import fonds d'écran
######################
wget "${URLWP}"
cp wall/* $HOME/Images/
echo "Fonds d'écran importés dans le dossier Images"

# ARDUINO
#########
# IDE
if [ "$ARCHITECTURE" = "x86_64" ]; then
    echo "Téléchargement de l'IDE arduino pour l'architecture 64bit"
    wget $ARDU64
    
else
    echo "Téléchargement de l'IDE arduino pour architecture 32bit"
    wget $ARDU32
fi

tar xvf arduino-*
mv arduino-* $REPINSTALL
bash $REPINSTALL/arduino-*/install.sh
sudo adduser $USER dialout
echo "L'IDE arduino V1.6 est installé, un raccourci est disponible sur le bureau et dans le menu"
# ArduBlock
echo "Téléchargement d'ArduBlock"
wget $URLARDUBLOCK
mkdir -p $DIRARDUBLOCK
cp ardublock* $DIRARDUBLOCK/ardublock-all.jar
echo "ArduBlock a été ajouté à l'IDE Arduino"


# FREECAD
#########
echo "Installation de Freecad"
sudo apt-get install -y python-pivy python-yaml python-numpy python-pyside
sudo add-apt-repository -y ppa:freecad-community/ppa
sudo apt-get update
sudo apt-get install -y freecad freecad-doc
# Plugins
echo "Installation des plugins pour Freecad"
sudo apt-get install -y $FREECADPLUGINS
cp /usr/share/applications/freecad.desktop $HOME/Bureau/
chmod +x $HOME/Bureau/freecad.desktop
echo "FreeCad et ses extensions sont installés, un raccourci est disponible sur le bureau et dans le menu"


#Scratch
########
echo "Installation de Scratch"
sudo apt-get install -y scratch
cp /usr/share/applications/scratch.desktop $HOME/Bureau/
chmod +x $HOME/Bureau/scratch.desktop
echo "Scratch est installé, un raccourci est disponible sur le bureau et dans le menu"

#XOD
######
echo "Installation de XOD"
echo "Vérification de la compatibilité"
if [ "$ARCHITECTURE" = "x86_64" ]; then
    echo "Téléchargement de XOD pour l'architecture 64bit"
    wget $URLXOD
    sudo dpkg -i xod-*
    sudo apt-get install -f -y
	cp /usr/share/applications/xod-client-electron.desktop $HOME/Bureau/
	chmod +x $HOME/Bureau/xod-client-electron.desktop
	echo "XOD est installé, un raccourci est disponible sur le bureau et dans le menu"
    
else
    echo "Téléchargement et installation impossible, pas de version de XOD pour cette architecture"
fi


#Fritzing
#########
echo "Installation de Fritzing"
sudo apt-get install -y fritzing
cp /usr/share/applications/fritzing.desktop $HOME/Bureau/
chmod +x $HOME/Bureau/fritzing.desktop
echo "Fritzing est installé, un raccourci est disponible sur le bureau et dans le menu"


#Processing
###########
echo "Installation de Processing"
if [ "$ARCHITECTURE" = "x86_64" ]; then
    echo "Téléchargement de Processing pour l'architecture 64bit"
    wget $PROCESSING64
    
else
    echo "Téléchargement de Processing pour architecture 32bit"
    wget $PROCESSING32
fi

tar xvf processing-*
cp -r processing-* $REPINSTALL
cd processing-* && bash install.sh
echo "Processing est installé, un raccourci est disponible sur le bureau et dans le menu"

# Cleanup
rm -rf $REPTMP

exit 0;
